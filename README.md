# CTW

## Description

Let's say you have, in your story or article or whatever, a sentence
like this:

> I had lived in Bura twenty years by that point, having moved from
> the place of my birth, Callow, a small city about a day away by
> horse. I had been quite successful, having been ...

Those two "having"s -- yeah, I agree, ctw, they're crap. This program
will, when run on a file, show you these repetitions in this format:

```

	my-story/src/story.md:8: "Bura", previous: 2:49
	my-story/src/story.md:10: "having", previous: 3:14


```

The first part is file-name:line:col. The bit in quotes is the
offending word. "previous: x:xx" is the line and col where that
word was previously found.

## stdin

You have it read from a pipe if you pass "-", eg `cat /tmp/story.md |
ctw -`. It'll print "stdin" instead of the non-existent filename.

## Ignored words

The program doesn't flag up every word. It ignores the most common
English words. There's a hard-coded list, and you can pass one list
with --ignored-words.

## Ignoring individual words

You can stick a backslash at the start of words if you want them to be
ignored. A mad thing to do, marking up your prose like that. But
frankly, you'll need to sometimes.

## --min-matches

You can specify a minimum threshold for printing matches. eg,
--min-matches=1 will only print a match if it encounters one match
before the match it's on. You try experimenting with this and
--distance. You could pass a long search distance and a high
-min-matches.

## CTW and your editor

A program like this is a bit painful to use if you don't get your
editor involved.

So here's a simple Vim function and command to put the output of ctw
into a Vim quickfix list.

```Vim

func! CTW ()
	let l:saved_errorformat = &errorformat
	let &errorformat = '%f:%l:%c %m'
	let l:saved_makeprg = &makeprg
	set makeprg=ctw\ %
	echom &makeprg
	make
	let &errorformat = l:saved_errorformat
	let &makeprg = l:saved_makeprg
endfunc

" Type :CTW to run CTW ()
command CTW call CTW ()

" A normal-mode mapping to call CTW ()
nnoremap <leader>C :call CTW()<cr>


```

I won't make this into a plugin. At some point maybe I'll write a
[Syntastic](https://github.com/vim-syntastic/syntastic) checker.

## Improvements that can be made

### Path-printing

ctw prints out the whole path for every line. It won't be too long
most of the time, cause you won't be running the program from /. But
it'd be nice to shorten it. gcc seems to shorten paths like this:

	../src/story.md
		Blah blah blah undefined variable

But Vim interprets that ../ quite rightly as meaning jump to the
directory before src. So, fuck knows what Syntastic is doing to fix up
the path.

### Ignoring stuff in code blocks

Having it ignore stuff in code blocks.

## Fun

Now, for fun, let's run ctw on (an earlier version of) this very file.

Actually, I've done it on another file, where I removed all the code
and examples.

```

	/tmp/ha:7: "ctw", match on 2
	/tmp/ha:11: "file", match on 8
	/tmp/ha:12: "word", match on 12
	/tmp/ha:14: "match", match on 12
	/tmp/ha:15: "Bura", match on 14
	/tmp/ha:17: "I'll", match on 17
	/tmp/ha:23: "words", match on 20
	/tmp/ha:24: "words", match on 20
	/tmp/ha:23: "word", match on 22
	/tmp/ha:24: "word", match on 22
	/tmp/ha:24: "words", match on 23
	/tmp/ha:29: "editor", match on 26
	/tmp/ha:29: "editor", match on 26
	/tmp/ha:29: "editor", match on 29
	/tmp/ha:31: "Vim", match on 29
	/tmp/ha:32: "Vim", match on 29
	/tmp/ha:37: "Vim", match on 29
	/tmp/ha:32: "Vim", match on 31
	/tmp/ha:37: "Vim", match on 31
	/tmp/ha:37: "function", match on 31
	/tmp/ha:37: "Vim", match on 32
	/tmp/ha:37: "Syntastic", match on 35
	/tmp/ha:44: "path", match on 42
	/tmp/ha:43: "won't", match on 42
	/tmp/ha:44: "shorten", match on 44
	/tmp/ha:47: "Blah", match on 47
	/tmp/ha:47: "Blah", match on 47
	/tmp/ha:47: "blah", match on 47
	/tmp/ha:54: "Fun", match on 52

```


But look at this:

> ## CTW and your editor
>
> A program like this is a bit painful to use if you don't get your
> editor involved. I mean an editor like Vim, not a human being.
>
> So here's a simple Vim function and command to put the output of ctw
> into a Vim quickfix list.

ctw correctly notes I've used editor three times. That might look
wrong, but it's right. First it see the first editor, tries to look
for matches, finds two, then it sees the second one, finds one. I
won't try to do anything cleverer than that.

But the point is, are these "editor"s bad? It's debatable. And here's
where this program shows how its different to a compiler. A compiler
shows you mistakes -- you fix those mistakes and the problem goes
away. But here you have to use your judgement.

I won't add anything fancy like regexes to suppress certain matches.
It'd be a nightmare to use, though you could write a wrapper for your
editor to make it easier.

