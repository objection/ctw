#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <argp.h>
#include <useful.h>
#include <err.h>
#include <stdbool.h>
#include <unistd.h>
#include "../lib/cat/cat.h"
#include "../lib/split/split.h"

#define $max(a,b) \
	({ __typeof__ (a) _a = (a); \
	 __typeof__ (b) _b = (b); \
	 _a > b? _a : _b; })

#define $min(a,b) \
	({ __typeof__ (a) _a = (a); \
	 __typeof__ (b) _b = (b); \
	 _a > b? _b : _a; })
#define $strncasematch !strncasecmp
#define $strmatch !strcmp
#define $n(x) ((sizeof(x)/sizeof((x)[0])) / ((size_t)(!(sizeof(x) % sizeof((x)[0])))))
#define $get_int(_res, str, e) \
	({ \
		char *end; \
		errno = 0; \
		_res = strtol (str, &end, 0); \
		errno || (e? end != e: *end != '\0')? 1: 0; \
	})

enum w_type {
	W_TYPE_WORD,
	W_TYPE_PUNCT,
};

struct w {
	char *s, *e;
	enum w_type type;
	int line;
	char *line_start; // For working out cols
};

struct args {
	int search_distance;
	char **f_paths;
	size_t n_f_paths;
	char **ignored_words;
	size_t n_ignored_words;
	int min_matches;
} args = {.search_distance = 40};

// Don't bother to sort this beforehand. It has to be sorted
// case-insensitively. You could do it, but you'd forget and sort it
// in a normal way and get errors. Having said that, I've said this,
// haven't I? So you know.
char *common_words[] = {
	"a", "about", "after", "all", "also", "an", "and", "any" "as",
	"as", "at", "back", "be", "because", "but", "by", "can", "come",
	"could", "day", "do", "even", "first", "for", "from", "get",
	"give", "go", "good", "had", "had", "have", "he", "her", "him",
	"his", "how", "I", "if", "is", "in", "into", "it", "its", "just",
	"know", "like", "look", "make", "me", "most", "my", "new", "no",
	"not", "now", "of", "on", "one", "only", "or", "other", "our",
	"out", "over", "people", "said", "say", "see", "she", "so",
	"some", "take", "than", "that", "the", "their", "them", "then",
	"there", "these", "they", "think", "this", "time", "to", "two",
	"up", "us", "use", "want", "was", "way" "we", "well", "what",
	"when", "which", "who", "will", "with", "work", "would", "year",
	"you", "your",
};



// Returns 0 for '\0'. There's not TYPE_0.
int get_w (struct w *w) {
	(*w).s = (*w).e;
	while (isblank (*(*w).s)) (*w).s++;
	if (*(*w).s == '\0') return 1;
	(*w).e = (*w).s + 1;
	if (*(*w).s == '\n') {
		(*w).line++;
		(*w).line_start = (*w).s;
		get_w (w);
	}
	if (!isalnum (*(*w).s)) {
		(*w).type = W_TYPE_PUNCT;
		return 0;
	}
	while (!isspace (*(*w).e)) {
		// Punctuation in the middle of words is part of the word, eg
		// this-and-that or 34.5667. With this, This,that will also be a
		// word but that's fine, I think.
		if (ispunct (*(*w).e) && !isalnum (*((*w).e + 1)))
			break;

		(*w).e++;
	}
	(*w).type = W_TYPE_WORD;
	return 0;
}

// This isn't cmping strs, right? I can't seem to just
// return strcmp ((const char *) aa, (const char *) bb);
int cmp_str (const void *a, const void *b) {
	const char **aa = (const char **) a;
	const char **bb = (const char **) b;
	return strcasecmp (*aa, *bb);
}

#if 0
char *shorten_path (char *f_path) {
	char *res;
	char *p = strchr (f_path, '\0');
	int i = 0;
	while (p >= f_path) {
		if (*p == '/') i++;
		if (i == 2) break;
		p--;
	}
	if (i < 2) return strdup (f_path);
	asprintf (&res, "..%s", p);
	return res;
}
#endif

static inline void print_if_valid_match (struct w w, struct w search_w,
		char *w_buf, char *shortened_path, int *n_matches) {
	if ($strncasematch (search_w.s, w.s,
				(int) $max (search_w.e - search_w.s, w.e - w.s))) {

		// I could save this sprintf with a bsearch_r. There's
		// no bsearch_r in glibc as far as I can see. It is in
		// gnu's libiberty. I could paste it in here but I won't.

		sprintf (w_buf, "%.*s", (int) (w.e - w.s), w.s);

		if (!bsearch (&w_buf, common_words, $n (common_words),
					sizeof *common_words, cmp_str)) {

			if (*n_matches >= args.min_matches)
				printf ("%s:%d:%zu \"%s\", previous: %d:%zu\n",
						shortened_path, search_w.line, search_w.s -
						search_w.line_start,
						w_buf, w.line, w.s - w.line_start);
			(*n_matches)++;
		}
	}
}

static void parse_file (char *f_path) {
	// Optimisations: could reuse this buffer, but cat doesn't do that.
	char *buf;
	if ($strmatch (f_path, "-")) {
		// Really, some kind of return value should be tested, but
		// cat_fps doesn't return anything -- it should.
		cat (&buf, (FILE **) &stdin, 1, 0);
		f_path = "stdin";
	} else if (cat (&buf, (const char **) &f_path, 1, 0))
		err (1, "Couldn't read %s", f_path);
	// Note .line is set to 1. This is what Richard Stallman told
	// me to do.
	struct w w = {.e = buf, .line = 1, .line_start = buf};

	// This really needs to be malloced because of bsearch. I think I
	// could cast it, but I hear casts aren't free, cpu-wise.
	char *w_buf = malloc (256);

	/* char *shortened_path = shorten_path (f_path); */
	while (!get_w (&w)) {
		// Skip escaped words
		if (w.s - 1 > buf && *(w.s - 1) ==  '\\')
			continue;
		if (w.type != W_TYPE_PUNCT) {
			struct w search_w = w;
			int n_matches = 0;
			for (int i = 0; i < args.search_distance; i++) {
				if (get_w (&search_w)) break;
				// Skip escaped words
				if (search_w.s - 1 > buf && *(search_w.s - 1) ==  '\\')
					continue;
				print_if_valid_match (w, search_w, buf, f_path, &n_matches);
			}
		}
	}
	free (w_buf);
	free (buf);
}

static error_t parse_opt (int key, char *arg, struct argp_state *state) {
	switch (key) {
		case 'd':
			if ($get_int (args.search_distance, arg, NULL))
				errx (1, "%s is not an integer", arg);
			break;
		case 'i': {
			char *buf;
			if (cat (&buf, (const char **) &arg, 1, 0))
				errx (1, "Couldn't read ignored words file %s", arg);
			args.ignored_words = split (buf, NULL, (char *[]) {"\n"}, 1,
					&args.n_ignored_words, 1);
		}
			break;
		case 'm':
			if ($get_int (args.min_matches, arg, NULL))
				errx (1, "%s is not an integer", arg);
			break;
		case ARGP_KEY_INIT:
			args.f_paths = malloc (sizeof *args.f_paths *
					(*state).argc - 1);
			break;
		case ARGP_KEY_ARG:
			// Not strduping.
			args.f_paths[args.n_f_paths++] = arg;
			break;
		case ARGP_KEY_END:
			if (!args.ignored_words) {
				args.ignored_words = common_words;
				args.n_ignored_words = $n (common_words);
			}
			if (args.n_f_paths == 0) argp_usage (state);
			break;
		default:
			break;
	}
	return 0;
}

int main (int argc, char **argv) {
	// Go word by word
	// go down a few lines, checking for the word
	struct argp argp = {
		.options = (struct argp_option[]) {
			{"distance", 'd', "INTEGER", 0, "\
Number of words to after word to check for a match (default: 40)"},
			{"ignored-words", 'i', "PATH", 0, "\
File of words to ignore, one per line"},
			{"min-matches", 'm', "INTEGER", 0, "\
Number of repeated words to ignore"},
			{0},
		},
		parse_opt,
		"FILES (\"-\" for stdin)", "\
Check FILES for words repeated too soon\v\
Note on output format: it's the expected way, if you've used GNU tools -- \
file:line:column. \
Tabs are treated as one character. It doesn't try to translate that into \
columns. That'd be more work.\n\
\n\
Note that matching is done case-insensatively. I doubt I'll put in an option to change \
that."

	};
	argp_parse (&argp, argc, argv, 0, 0, NULL);
	$qsort (args.ignored_words, args.n_ignored_words, cmp_str);
	for (int i = 0; i < args.n_f_paths; i++)
		parse_file (args.f_paths[i]);
}


